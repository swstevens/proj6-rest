import os
from flask import Flask, redirect, url_for, request, render_template
from pymongo import MongoClient

app = Flask(__name__)

client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'], 27017)
db = client.tododb

@app.route('/')
def todo():
    _items = db.tododb.find()
    items = [item for item in _items]

    return render_template('todo.html', items=items)

@app.route('/new', methods=['POST'])
def new():
    db.tododb.drop()
    names = request.form.getlist('name')
    descriptions = request.form.getlist('description')
    for i in range(len(names)):
        
        item_doc = {
            'name': names[i],
            'description': descriptions[i]
        }
        db.tododb.insert_one(item_doc)
    app.logger.info(request.form.getlist('name'))
    print(item_doc['name'])
    print(item_doc['description'])
    #if item_doc['name'] is not "" or item_doc['description'] is not "":
        #db.tododb.insert_one(item_doc)

    return redirect(url_for('todo'))

if __name__ == "__main__":
    app.run(host='0.0.0.0', debug=True)
