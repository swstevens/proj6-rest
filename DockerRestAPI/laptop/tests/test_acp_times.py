import acp_times
import arrow

def test_first_control():
    test_time = arrow.get("2020-10-10 12:00:00", "YYYY-MM-DD HH:mm:ss")
    assert open_time(0, 200, test_time) == test_time
    assert close_time(0, 400, test_time) == test_time.shift(hours=+1).isoformat()
    assert close_time(0, 600, test_time) == arrow.get("2020-10-10 13:00:00", "YYYY-MM-DD HH:mm:ss")

def test_200():
    start_time = arrow.get("2020-10-10 17:53:00", "YYYY-MM-DD HH:mm:ss")
    finish_time = arrow.get("2020-10-11 01:30:00", "YYYY-MM-DD HH:mm:ss")
    test_time = arrow.get("2020-10-10 12:00:00", "YYYY-MM-DD HH:mm:ss")
    # the results for the 200 km control point should be the same
    # regardless of total brevet distance
    assert open_time(200, 200, test_time) == start_time
    assert open_time(200, 400, test_time) == start_time
    assert open_time(200, 600, test_time) == start_time
    assert open_time(200, 1000, test_time) == start_time

    assert close_time(200, 200, test_time) == finish_time
    assert close_time(200, 400, test_time) == finish_time
    assert close_time(200, 600, test_time) == finish_time
    assert close_time(200, 1000, test_time) == finish_time

def test_1200():
    test_time = arrow.get("2020-10-10 12:00:00", "YYYY-MM-DD HH:mm:ss")
    # as per the ruleset, control points past the toal brevet dist
    # are treated as if they are at the max brevet dist
    assert open_time(1000,1000, test_time) == open_time(1200,1000,test_time)
    assert close_time(1000,1000, test_time) == open_time(1200,1000,test_time)
