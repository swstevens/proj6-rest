<html>
    <head>
        <title>CIS 322 REST-api demo: Laptop list</title>
    </head>

    <body>
        <h1>Parsable Datas</h1>
        <ul>
	<h1>List of forAll</h1>
	    <?php
	    $json = file_get_contents('http://laptop-service/listAll');
	    $obj = json_decode($json);
	    echo "Listed as Close, Open";
	    foreach($obj as $result) {
		    echo "<li>";
		    foreach($result as $entry) {
		    	echo "$entry, ";
		    }
		    echo "</li>";
	    }
	    ?>
	<h1>ForAll CSV</h1>
	    <?php
	    $json = file_get_contents('http://laptop-service/listAll/csv');
	    $obj = json_decode($json);
	    $obj = nl2br($obj);
	    echo "Listed as Close, Open", "<br>";
	    echo "\r\n";
	    echo $obj;
	    ?>

	<h1>List of forOpenOnly</h1>
	    <?php
	    $json = file_get_contents('http://laptop-service/listOpenOnly');
	    $obj = json_decode($json);
	    foreach($obj as $result) {
		    echo "<li>";
		    foreach($result as $entry) {
		    	echo "$entry";
	    	    }
	            echo "</li>";
	    }
?>

	<h1>OpenOnly CSV</h1>
	    <?php
	    $json = file_get_contents('http://laptop-service/listOpenOnly/csv');
	    $obj = json_decode($json);
	    $obj = nl2br($obj);
	    echo $obj;
	    ?>
	<h1>List of forCloseOnly</h1>
	    <?php
	    $json = file_get_contents('http://laptop-service/listCloseOnly');
	    $obj = json_decode($json);
	    foreach($obj as $result) {
		    echo "<li>";
		    foreach($result as $entry) {
		    	echo "$entry";
	    	    }
	            echo "</li>";
	    }	    
	    ?>
	<h1>CloseOnly CSV</h1>
	    <?php
	    $json = file_get_contents('http://laptop-service/listCloseOnly/csv');
	    $obj = json_decode($json);
	    $obj = nl2br($obj);
	    echo $obj;
	    ?>	

	<h1>forAll Limit 1</h1>
	    <?php
	    $json = file_get_contents('http://laptop-service/listAll/json?top=1');
	    $obj = json_decode($json);
	    echo "Listed as Close, Open";
	    foreach($obj as $result) {
		    echo "<li>";
		    foreach($result as $entry) {
		    	echo "$entry, ";
		    }
		    echo "</li>";
	    }
	    ?>
	<h1>ForAll CSV Limit 1</h1> 
	    <?php
	    $json = file_get_contents('http://laptop-service/listAll/csv?top=1');
	    $obj = json_decode($json);
	    $obj = nl2br($obj);
	    echo "Listed as Close, Open", "<br>";
	    echo "\r\n";
	    echo $obj;
	    ?>

	<h1>OpenOnly Limit 1</h1>
	    <?php
	    $json = file_get_contents('http://laptop-service/listOpenOnly/json?top=1');
	    $obj = json_decode($json);
	    foreach($obj as $result) {
		    echo "<li>";
		    foreach($result as $entry) {
		    	echo "$entry";
	    	    }
	            echo "</li>";
	    }
?>

	<h1>OpenOnly CSV limit 1</h1>
	    <?php
	    $json = file_get_contents('http://laptop-service/listOpenOnly/csv?top=1');
	    $obj = json_decode($json);
	    $obj = nl2br($obj);
	    echo $obj;
	    ?>
	<h1>CloseOnly Limit 1</h1>
	    <?php
	    $json = file_get_contents('http://laptop-service/listCloseOnly/json?top=1');
	    $obj = json_decode($json);
	    foreach($obj as $result) {
		    echo "<li>";
		    foreach($result as $entry) {
		    	echo "$entry";
	    	    }
	            echo "</li>";
	    }	    
	    ?>
	<h1>CloseOnly CSV Limit 1</h1>
	    <?php
	    $json = file_get_contents('http://laptop-service/listCloseOnly/csv?top=1');
	    $obj = json_decode($json);
	    $obj = nl2br($obj);
	    echo $obj;
	    ?>	


        </ul>
    </body>
</html>
