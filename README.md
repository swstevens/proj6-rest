# Project 6: Brevet time calculator service

Simple listing service from project 5 stored in MongoDB database. Example provided by Michal Young and Ram Durairajan. 
Built upon by Shea Stevens, sstevens@cs.uoregon.edu.

## What is in this repository

Project 5's base code, in which a user can input distances for brevet control times. When they click on submit, the entries are stored in the database, and, when requested, are returned to the user at another url. The control times are calculated instantly upon entering the distance into the text field.

A RESTful API is built on top of project 5 and returns either a JSON or CSV formatted response of controle times inputted at "http://<host:port>/". The default port is 5000 for the Control Time input and display. The options for API responses are as follows:

* The three basic APIs are:
    * "http://<host:port>/listAll" returns all open and close times in the database (in the order of close then open)
    * "http://<host:port>/listOpenOnly" returns open times only
    * "http://<host:port>/listCloseOnly" returns close times only

* For CSV and JSON specification use the following APIs: 
    * "http://<host:port>/listAll/csv" returns all open and close times in CSV format (in the order of close then open)
    * "http://<host:port>/listOpenOnly/csv" returns open times only in CSV format
    * "http://<host:port>/listCloseOnly/csv" returns close times only in CSV format

    * "http://<host:port>/listAll/json" returns all open and close times in JSON format (in the order of close then open)
    * "http://<host:port>/listOpenOnly/json" returns open times only in JSON format
    * "http://<host:port>/listCloseOnly/json" returns close times only in JSON format

* For Limiting the number of Control time returned, use the following API style:

    * "http://<host:port>/listOpenOnly/csv?top=3" returns top 3 open times only (in ascending order) in CSV format 
    * "http://<host:port>/listOpenOnly/json?top=5" returns top 5 open times only (in ascending order) in JSON format
    * "http://<host:port>/listCloseOnly/csv?top=6" returns top 5 close times only (in ascending order) in CSV format
    * "http://<host:port>/listCloseOnly/json?top=4" returns top 4 close times only (in ascending order) in JSON format

* To Access the Consumer website, once again navigate to:
    * "http://<host:port>/"
        * make sure to change the port to 5001, the setting for the consumer website